const express = require("express");
const app =express();
const dotenv = require("dotenv");

dotenv.config({ path: "./config.env"});





app.get('/settin',(req,res) =>{
    res.send({
        language:req.query.language,
        sound:req.query.sound,
        camara:req.query.camara
    })
})


// Mongodb Connected
require("./config/db");


// Init Middleware
app.use(express.json());

// Define Routes

app.use('/api/users',require('./routes/api/user.routes'));

const PORT = process.env.PORT || 5000;

app.listen(PORT,()=>{
    console.log(`Server is running on :${PORT}`);
});