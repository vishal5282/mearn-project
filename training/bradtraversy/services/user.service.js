var User = require('../models/user.model');



exports.getUsers = async function (req,res) {

    try {
        var users = await User.find()
        return users;
    } catch (e) {
        // Log Errors
        throw Error('Error while Paginating Users')
    }
}