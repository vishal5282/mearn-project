const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const {check,validationResult} = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const gravatar = require("gravatar");
const normalize = require("normalize-url");

// @route    POST api/users
// @desc     Register user
// @access   Public

router.post("/",
    [

        check("name", "Name is require").notEmpty(),
        check("email", "email is require").isEmail(),
        check("password", "password is require").isLength({min:6})
    ],

    async(req,res)=>{

        const errors = validationResult(req);

        if(!errors.isEmpty()){
            return res.status(400).json({errors:errors.array()});
        }

        const {name,email,password} = req.body;

        // console.log(req.body);
        // res.json(req.body);

        try{

            let user = await User.findOne({email});


            if(user){
                return res.status(400).json({errors:[{msg:"User is already exists"}]})
            }

            const avatar = normalize(
                gravatar.url(email, {
                  s: '200',
                  r: 'pg',
                  d: 'mm'
                }),
                { forceHttps: true }
              );

              user = new User({
                  name,
                  email,
                  avatar,
                  password
              });

              const salt = await bcrypt.gensalt(10);

              user.password=await bcrypt.hasg(password,salt);

              await user.save();

              const payload ={
                  user:{
                      id:user.id
                  }
              };
            

              jwt.sign(
                  payload,
                  process.env.JWTSECRET,
                  {
                      expiresIn:"360000"
                  },
                  (err,token) =>{
                      if(err) throw err;
                      res.json({token});
                  }
                );
        }catch(err){
            console.error(err.message);
            res.status(500).json("Server Error");
        }
    }

);


router.post("/register",(req,res) =>{
    console.log(req.body);
    res.json(req.body);
});

module.exports = router;