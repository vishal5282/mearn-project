var express = require('express');
var router = express.Router();

var UserController = require('../../controllers/user.controller')

router.post('/', UserController.getUsers)

module.exports = router;