const mongoose = require("mongoose");


const UserSchema  = new mongoose.Schema({
    name:{
        type:String,
        require:true
    },
    language:{
        type:String,
        default:"English",
        require:true
    },
    sound:{
        type:Boolean,
        require:true
    },
    camara:{
        type:String,
        url:"https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png",
        require:true
    }
})

const User = mongoose.model('User', UserSchema)

module.exports = User;