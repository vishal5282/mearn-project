const jwt = require("jsonwebtoken");
const key = process.env.JWTSECRET;



module.exports = function (req,res,next){
    // GET token from header

    const token = req.header("x-auth-token");


    // check if not token

    if(!token){
        return res.status(401).json({msg:"No Token , authorization denied"});
    }

    // verify token

    try{
        jwt.verify(token,key,(error,decoded)=>{
            if(error){
                return res.status(401).json({msg:"Token is not valid"});
            }else{
                req.user = decoded.user;
                next();
            }

        });

    }catch(err){
        console.error("aomething wrong eith auth middlware");
        res.status(500).json({msg:"Server Error"});

    }
};